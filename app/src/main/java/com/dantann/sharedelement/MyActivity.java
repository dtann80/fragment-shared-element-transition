package com.dantann.sharedelement;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class MyActivity extends Activity {

    protected static Fragment mainFragement, detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        if (savedInstanceState == null) {


            Transition tran = createTransition();

            mainFragement = new MainFragment();
            mainFragement.setSharedElementEnterTransition(tran);
            detailFragment = new OtherFragment();
            detailFragment.setSharedElementEnterTransition(tran);
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFragement,mainFragement.getClass().getSimpleName())
                    .add(R.id.container, detailFragment,detailFragment.getClass().getSimpleName())
                    .hide(detailFragment)
                    .commit();
        }
    }

    public Transition createTransition() {
        return  new TransitionSet()
                .addTransition(new ChangeBounds())
                .addTransition(new ChangeImageTransform());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class MainFragment extends Fragment implements View.OnClickListener {

        public ImageView imageView;

        public MainFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            rootView.findViewById(R.id.button).setOnClickListener(this);
            imageView = (ImageView)rootView.findViewById(R.id.imageView);

            return rootView;
        }

        @Override
        public void onClick(View v) {
            getFragmentManager().beginTransaction()
                    .addSharedElement(imageView,"transition_imageView")
                    .hide(MyActivity.mainFragement)
                    .show(MyActivity.detailFragment)
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class OtherFragment extends Fragment implements View.OnClickListener {

        public ImageView imageView;

        public OtherFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
            rootView.findViewById(R.id.button);
            rootView.findViewById(R.id.button).setOnClickListener(this);
            imageView = (ImageView)rootView.findViewById(R.id.imageView);

            return rootView;
        }

        @Override
        public void onClick(View v) {
            getFragmentManager().beginTransaction()
                    .addSharedElement(imageView,"transition_imageView")
                    .hide(MyActivity.detailFragment)
                    .show(MyActivity.mainFragement)
                    .commit();
        }
    }
}
